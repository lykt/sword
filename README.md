# SWORD utility

SWORD is command line utility that sorts words by typing effort and calculates keyboard layout properties using frequency dictionary.

To generate brief report for all layouts run
```
node sword
```
Output includes list of layouts with layout properties.

To genarate more complete report for specific layout pass layout name as parameter, like
```
node sword orthostat
```

Output includes list of words ordered by typing effort and layout properties.
