let words = require('../data/en.json');
let props = require('./props.json');
const keys = require('./keys.json');
const layouts = require('./layouts.json');
let results = JSON.parse('{}'), filter = process.argv.length > 2;
for (var l in layouts) {
    if (!filter || layouts[l].name == process.argv[2]) {
        rank(l, filter);
    }
}
console.log(JSON.stringify(results, null, 2));

function rank(l, all) {
    let layout = layouts[l].name, keymap = layouts[l].keymap, ranked = JSON.parse('{}'), adaptive = layouts[l].special, patterns = [];
    if (adaptive) {
        for (let key of adaptive) {
            patterns.push(new RegExp(keys[key], 'g'));
        }
    }
    for (let hand of[0, 1]) {
        for (let f of[0, 1, 2, 3, 4]) {
            props.hands[hand].fingers[f].dict = {
                "top": 0,
                "bottom": 0,
                "home": 0,
                "keyrepeat": 0,
                "samefinger": 0,
                "jump": 0,
                "lateral": 0,
                'freq': 0
            };
        }
        props.hands[hand].dict = {};
        for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand']) {
            props.hands[hand].dict[prop] = 0;
        }
    }
    let effort = 0, size = 0, gain = 0;
    for (let word of Object.keys(words)) {
        for (let hand of[0, 1]) {
            for (let f of[0, 1, 2, 3, 4]) {
                props.hands[hand].fingers[f].word = {
                    "top": 0,
                    "bottom": 0,
                    "home": 0,
                    "keyrepeat": 0,
                    "samefinger": 0,
                    "jump": 0,
                    "lateral": 0,
                    'freq': 0
                };
            }
            props.hands[hand].word = {};
            for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand']) {
                props.hands[hand].word[prop] = 0;
            }
        }
        let a = '', b = (word.length > 8) ? "" : "␣", adapted = word;
        if (adaptive) {
            for (let key = 0; key < adaptive.length; key++) {
                let sl = adapted.length;
                adapted = adapted.replace(patterns[key], adaptive[key]);
                gain += (sl - adapted.length) * words[word];
            }
        }
        adapted += b;
        for (var c of adapted) {
            if (!keymap[c]) {
                console.log('Character ' + c + ' is not in keymap');
            }
            if (typeof keymap[c].h != "undefined") {
                props.hands[keymap[c].h].fingers[keymap[c].f].word.freq++;
            } else {
                props.hands[(keymap[b].h + 1) % 2].fingers[keymap[c].f].word.freq++;
            }
            if (keymap[c].r == 0) {
                props.hands[keymap[c].h].fingers[keymap[c].f].word.top++;
                if (b && keymap[b].r == 2) {
                    if (keymap[b].f == keymap[c].f) {
                        props.hands[keymap[c].h].fingers[keymap[c].f].word.jump += 3;
                    } else {
                        props.hands[keymap[c].h].word.jump += (keymap[c].h == keymap[b].h) ? 2 : 1;
                    }
                }
            } else if (keymap[c].r == 2) {
                props.hands[keymap[c].h].fingers[keymap[c].f].word.bottom++;
                if (b && keymap[b].r == 0) {
                    if (keymap[b].f == keymap[c].f) {
                        props.hands[keymap[c].h].fingers[keymap[c].f].word.jump += 3;
                    } else {
                        props.hands[keymap[c].h].word.jump += (keymap[c].h == keymap[b].h) ? 2 : 1;
                    }
                }
            } else if (!keymap[c].o && !keymap[c].i) {
                if (typeof keymap[c].h != "undefined") {
                    props.hands[keymap[c].h].fingers[keymap[c].f].word.home++;
                } else {
                    props.hands[(keymap[b].h + 1) % 2].fingers[keymap[c].f].word.home++;
                }
            }
            if (keymap[c].i) {
                props.hands[keymap[c].h].fingers[keymap[c].f].word.lateral += keymap[c].i;
                if (b && keymap[b].o) {
                    props.hands[keymap[c].h].word.shake += keymap[c].i + keymap[b].o - 1;
                }
                if (b && !keymap[b].i && keymap[b].f == 0 && keymap[c].h == keymap[b].h) {
                    props.hands[keymap[c].h].word.torque += keymap[c].i;
                }
            }
            if (b && !keymap[c].i && keymap[c].f == 0 && keymap[c].h == keymap[b].h && keymap[b].i) {
                props.hands[keymap[c].h].word.torque += keymap[b].i;
            }
            if (keymap[c].o) {
                props.hands[keymap[c].h].fingers[keymap[c].f].word.lateral += keymap[c].o;
                if (b && keymap[b].i) {
                    props.hands[keymap[c].h].word.shake += keymap[c].o + keymap[b].i - 1;
                }
            }
            if (b && keymap[c].h == keymap[b].h) {
                if (keymap[c].f == keymap[b].f) {
                    if (b == c) {
                        props.hands[keymap[c].h].fingers[keymap[c].f].word.keyrepeat++;
                    } else // if(Math.abs(keymap[c].r - keymap[b].r) < 2)
                    {
                        props.hands[keymap[c].h].fingers[keymap[c].f].word.samefinger++;
                    }
                }
                if (a && keymap[c].h == keymap[a].h) {
                    props.hands[keymap[c].h].word.samehand++;
                    if ((keymap[c].f < keymap[b].f && keymap[a].f < keymap[b].f) || (keymap[c].f > keymap[b].f && keymap[a].f > keymap[b].f)) {
                        props.hands[keymap[c].h].word.redirect += 1 / 3;
                        if (keymap[a].f && keymap[b].f && keymap[c].f) {
                            props.hands[keymap[c].h].word.redirect += 2 / 3;
                        }
                    }
                }
            }
            a = b
            b = c;
        }
        let wl = adapted.length;
        for (let hand of[0, 1]) {
            for (let f of[0, 1, 2, 3, 4]) {
                for (let prop of Object.keys(props.hands[hand].fingers[f].word)) {
                    props.hands[hand].fingers[f].dict[prop] += props.hands[hand].fingers[f].word[prop] * words[word];
                }
            }
            for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand']) {
                props.hands[hand].dict[prop] += props.hands[hand].word[prop] * words[word];
            }
        }
        size += words[word] * wl;
        ranked[word] = calc() / wl;
    }
    results[layout] = {};
    if (all) {
        results[layout].name = layout;
        results[layout].keymap = keymap;
        results[layout].special = adaptive;
        let mapped = Object.keys(ranked).map(function(s) {
            return {
                str: s,
                effort: ranked[s]
            }
        });
        mapped.sort(function(a, b) {
            return b.effort - a.effort
        });
        let sorted = {};
        for (var m of mapped) {
            sorted[m.str] = m.effort;
        }
        results[layout].words = sorted;
    }
    results[layout].hands = [{}, {}];
    results[layout].fingers = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
    for (let hand of[0, 1]) {
        for (let f of[0, 1, 2, 3, 4]) {
            results[layout].fingers[(4 - f) * (1 - hand) + (5 + f) * hand].name = props.hands[hand].fingers[f].title;
            results[layout].fingers[(4 - f) * (1 - hand) + (5 + f) * hand].hand = hand ? 'right' : 'left';
            for (let prop of Object.keys(props.hands[hand].fingers[f].dict)) {
                props.hands[hand].fingers[f].dict[prop] /= size;
                results[layout].fingers[(4 - f) * (1 - hand) + (5 + f) * hand][prop] = props.hands[hand].fingers[f].dict[prop];
            }
        }
        for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand']) {
            props.hands[hand].dict[prop] /= size;
            results[layout].hands[hand][prop] = props.hands[hand].dict[prop];
        }
    }
    let shrink = (size - gain) / size;
    results[layout].shrink = shrink;
    results[layout].effort = calc(1, layout) * shrink;
    results[layout].rank = calc(2) * shrink;
}

function calc(pow, layout) {
    let sum = 0, p = 0;
    for (let hand of[0, 1]) {
        let hsum = 0, hp = 0;
        for (let f of[0, 1, 2, 3, 4]) {
            let fsum = 0, fp = 0;
            for (let prop of Object.keys(props.hands[hand].fingers[f].dict)) {
                if (!props[prop]) {
                    console.log(prop);
                }
                if (!pow) {
                    fsum += props.hands[hand].fingers[f].word[prop] * props.hands[hand].fingers[f].weight * props[prop].weight;
                } else {
                    fsum += Math.pow(props.hands[hand].fingers[f].dict[prop] * props.hands[hand].fingers[f].weight * props[prop].weight, pow);
                }
                fp++;
            }
            if (layout && pow) {
                results[layout].fingers[(4 - f) * (1 - hand) + (5 + f) * hand].effort = fsum / fp;
            }
            hsum += fsum;
            hp += fp;
        }
        for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand']) {
            if (!pow) {
                hsum += props.hands[hand].word[prop] * props[prop].weight;
            } else {
                hsum += Math.pow(props.hands[hand].dict[prop] * props[prop].weight, pow);
            }
            hp++;
        }
        if (layout && pow) {
            for (let prop of["top", "bottom", "home", "keyrepeat", "samefinger", "lateral", 'freq', 'jump']) {
                if (typeof results[layout].hands[hand][prop] == 'undefined') {
                    results[layout].hands[hand][prop] = 0;
                }
                for (let f of[0, 1, 2, 3, 4]) {
                    results[layout].hands[hand][prop] += props.hands[hand].fingers[f].dict[prop];
                }
            }
            results[layout].hands[hand].middle = results[layout].hands[hand].freq - results[layout].hands[hand].top - results[layout].hands[hand].bottom - props.hands[hand].fingers[0].dict.freq;
            results[layout].hands[hand].effort = hsum / hp;
        }
        sum += hsum;
        p += hp;
    }
    if (layout && pow) {
        for (let prop of['shake', 'jump', 'torque', 'redirect', 'samehand', "top", "bottom", 'middle', "home", "keyrepeat", "samefinger", "lateral", 'freq']) {
            results[layout][prop] = 0;
            for (let hand of[0, 1]) {
                results[layout][prop] += results[layout].hands[hand][prop];
            }
        }
    }
    return sum / p;
}
